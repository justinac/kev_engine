#ifndef CORE_H
#define CORE_H

#include <stdio.h>
#include <stdlib.h>

#include "core/util/types.h"
#include "core/util/dlists.h"
#include "core/util/math.h"
#include "core/gui/window.h"
#include "core/util/input.h"

#include "core/gfx/renderer.h"
#include "core/gfx/mesh.h"
#include "core/gfx/texture.h"
#include "core/gfx/shader.h"

#include "core/models/entity.h"
#include "core/models/scene.h"
#include "core/models/camera.h"

#include "core/physics/assorted.h"

#endif // CORE_H
