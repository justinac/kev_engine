#ifndef ASSORTED_H
#define ASSORTED_H

#include "../util/types.h"
#include "../models/entity.h"

boolean isColliding(Entity *entity1, Entity *entity2);

#endif // ASSORTED_H