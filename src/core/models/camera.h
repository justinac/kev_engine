#ifndef CAMERA_H
#define CAMERA_H

#include "../util/types.h"
#include "../util/math.h"

typedef struct {
    vec3 position;
    mat4 viewMatrix;
} Camera;

Camera camera_create(vec3 position);
void camera_lock(Camera *camera, vec3 target);

#endif // CAMERA_H