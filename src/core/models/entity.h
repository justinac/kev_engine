#ifndef ENTITY_H
#define ENTITY_H

#include "../util/types.h"
#include "../util/math.h"
#include "../gfx/mesh.h"

typedef struct {
    Mesh mesh;
    vec3 position;
    float rotation;
    float rotation_velocity;
    vec3 scale;
    vec3 size;
    vec3 velocity;
} Entity;

Entity entity_create(Mesh mesh, vec3 position, vec3 scale);
void entity_update(Entity *entity);

#endif // ENTITY_H