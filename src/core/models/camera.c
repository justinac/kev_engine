#include "camera.h"

Camera camera_create(vec3 position) {
    Camera camera;
    camera.position = position;
    mat4_identity(&camera.viewMatrix);

    return camera;
}

void camera_lock(Camera *camera, vec3 target) {
    mat4_translate(&camera->viewMatrix, (vec3){ -target.x, -target.y, 0});
}
