#ifndef MATH_H
#define MATH_H

#include <stdio.h>

#include "types.h"

void vec2_fill(vec2 *dest, float val);
void vec2_add(vec2 *dest, vec2 a, vec2 b);
void vec2_sub(vec2 *dest, vec2 a, vec2 b);
void vec2_div(vec2 *dest, vec2 a, vec2 b);
void vec2_mul(vec2 *dest, vec2 a, vec2 b);
void vec2_scale(vec2 *dest, vec2 a, float scale);
void vec2_normal(vec2 *dest, vec2 a);
void vec2_print(vec2 *dest);

void vec3_fill(vec3 *dest, float val);
void vec3_add(vec3 *dest, vec3 a, vec3 b);
void vec3_sub(vec3 *dest, vec3 a, vec3 b);
void vec3_div(vec3 *dest, vec3 a, vec3 b);
void vec3_mul(vec3 *dest, vec3 a, vec3 b);
void vec3_scale(vec3 *dest, vec3 a, float scale);
void vec3_normal(vec3 *dest, vec3 a);
void vec3_print(vec3 *dest);

void vec4_fill(vec4 *dest, float val);
void vec4_add(vec4 *dest, vec4 a, vec4 b);
void vec4_sub(vec4 *dest, vec4 a, vec4 b);
void vec4_div(vec4 *dest, vec4 a, vec4 b);
void vec4_mul(vec4 *dest, vec4 a, vec4 b);
void vec4_scale(vec4 *dest, vec4 a, float scale);

void mat4_fill(mat4 *dest, float value);
void mat4_copy(mat4 *dest, mat4 src);
void mat4_identity(mat4 *dest);
void mat4_mul(mat4 *dest, mat4 a, mat4 b);
void mat4_row(vec4 r, mat4 m, int i);
void mat4_translate(mat4 *dest, vec3 v);
void mat4_scale(mat4 *dest, vec3 v);
void mat4_rotate(mat4 *dest, float angle);
void mat4_orthographic(mat4 *dest, float left, float right, float top, float bottom, float znear, float zfar);
void mat4_print(mat4 *src);

#endif // MATH_H
