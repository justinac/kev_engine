#include "input.h"

boolean input_get_key(u32 key, u32 state) {
    return (glfwGetKey(glfwGetCurrentContext(), key) == state);
}
