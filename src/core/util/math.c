#include "math.h"

void vec2_fill(vec2 *dest, float val) {
    dest->x = val;
    dest->y = val;
}

void vec2_add(vec2 *dest, vec2 a, vec2 b) {
    dest->x = a.x + b.x;
    dest->y = a.y + b.y;
}

void vec2_sub(vec2 *dest, vec2 a, vec2 b) {
    dest->x = a.x - b.x;
    dest->y = a.y - b.y;
}

void vec2_div(vec2 *dest, vec2 a, vec2 b) {
    dest->x = a.x / b.x;
    dest->y = a.y / b.y;
}

void vec2_mul(vec2 *dest, vec2 a, vec2 b) {
    dest->x = a.x * b.x;
    dest->y = a.y * b.y;
}

void vec2_scale(vec2 *dest, vec2 a, float scale) {
    dest->x = a.x * scale;
    dest->y = a.y * scale;
}

void vec2_normal(vec2 *dest, vec2 a) {
    float modulus = sqrtf(a.x*a.x + a.y*a.y);
    dest->x = a.x/modulus;
    dest->y = a.y/modulus;
}

void vec2_print(vec2 *src) {
    printf("\naddr: %p\n", src);
    printf("x: %f\ny: %f\n", src->x, src->y);
}

void vec3_fill(vec3 *dest, float val) {
    dest->x = val;
    dest->y = val;
    dest->z = val;
}

void vec3_add(vec3 *dest, vec3 a, vec3 b) {
    dest->x = a.x + b.x;
    dest->y = a.y + b.y;
    dest->z = a.z + b.z;
}

void vec3_sub(vec3 *dest, vec3 a, vec3 b) {
    dest->x = a.x - b.x;
    dest->y = a.y - b.y;
    dest->z = a.z - b.z;
}

void vec3_div(vec3 *dest, vec3 a, vec3 b) {
    dest->x = a.x / b.x;
    dest->y = a.y / b.y;
    dest->z = a.z / b.z;
}

void vec3_mul(vec3 *dest, vec3 a, vec3 b) {
    dest->x = a.x * b.x;
    dest->y = a.y * b.y;
    dest->z = a.z * b.z;
}

void vec3_scale(vec3 *dest, vec3 a, float scale) {
    dest->x = a.x * scale;
    dest->y = a.y * scale;
    dest->z = a.z * scale;
}

void vec3_normal(vec3 *dest, vec3 a) {
    float modulus = sqrtf(a.x*a.x + a.y*a.y + a.x*a.z);
    dest->x = a.x/modulus;
    dest->y = a.y/modulus;
    dest->z = a.y/modulus;
}

void vec3_print(vec3 *src) {
    printf("\naddr: %p\n", src);
    printf("x: %f\ny: %f\nz: %f\n", src->x, src->y, src->z);
}

void vec4_fill(vec4 *dest, float val) {
    dest->x = val;
    dest->y = val;
    dest->z = val;
    dest->w = val;
}

void vec4_add(vec4 *dest, vec4 a, vec4 b) {
    dest->x = a.x + b.x;
    dest->y = a.y + b.y;
    dest->z = a.z + b.z;
    dest->w = a.w + b.w;
}

void vec4_sub(vec4 *dest, vec4 a, vec4 b) {
    dest->x = a.x - b.x;
    dest->y = a.y - b.y;
    dest->z = a.z - b.z;
    dest->w = a.w - b.w;
}

void vec4_div(vec4 *dest, vec4 a, vec4 b) {
    dest->x = a.x / b.x;
    dest->y = a.y / b.y;
    dest->z = a.z / b.z;
    dest->w = a.w / b.w;
}

void vec4_mul(vec4 *dest, vec4 a, vec4 b) {
    dest->x = a.x * b.x;
    dest->y = a.y * b.y;
    dest->z = a.z * b.z;
    dest->w = a.w * b.w;
}

void vec4_scale(vec4 *dest, vec4 a, float scale) {
    dest->x = a.x * scale;
    dest->y = a.y * scale;
    dest->z = a.z * scale;
    dest->w = a.w * scale;
}

void mat4_fill(mat4 *dest, float value) {
    int col, row;
    for (col = 0; col < 4; col++) {
        for (row = 0; row < 4; row++) {
            dest->m[col][row] = value;
        }
    }
}

void mat4_copy(mat4 *dest, mat4 src) {
    *dest = src;
}

void mat4_identity(mat4 *dest) {
    int col, row;
    for (col = 0; col < 4; col++) {
        for (row = 0; row < 4; row++) {
            dest->m[col][row] = (col == row) ? 1 : 0;
        }
    }
}

void mat4_mul(mat4 *dest, mat4 a, mat4 b) {
    mat4 temp;
    int k, r, c;
    for (c = 0; c < 4; ++c) {
        for (r = 0; r < 4; ++r) {
            temp.m[c][r] = 0.0f;
            for (k = 0; k < 4; ++k) {
                temp.m[c][r] += a.m[k][r] * b.m[c][k];
            }
        }
    }

    mat4_copy(dest, temp);
}

void mat4_row(vec4 r, mat4 m, int i) {
    int k;
    for (k = 0; k < 4; ++k) {
        r.v[k] = m.m[k][i];
    }
}

void mat4_translate(mat4 *dest, vec3 v) {
    dest->m[3][0] = v.x;
    dest->m[3][1] = v.y;
    dest->m[3][2] = v.z;
    dest->m[3][3] = 1;
}

void mat4_scale(mat4 *dest, vec3 v) {
    dest->m[0][0] = v.x;
    dest->m[1][1] = v.y;
    dest->m[2][2] = v.z;
    dest->m[3][3] = 1;
}

void mat4_rotate(mat4 *dest, float angle) {
    dest->m[0][0] = cosf(angle);
    dest->m[0][1] = -sinf(angle);

    dest->m[1][0] = sinf(angle);
    dest->m[1][1] = cosf(angle);
}

void mat4_orthographic(mat4 *dest, float left, float right, float top, float bottom, float znear, float zfar) {
    dest->m[0][0] = 2.f / (right - left);
    dest->m[0][1] = dest->m[0][2] = dest->m[0][3] = 0.f;

    dest->m[1][1] = 2.f / (top - bottom);
    dest->m[1][0] = dest->m[1][2] = dest->m[1][3] = 0.f;

    dest->m[2][2] = -2.f / (zfar - znear);
    dest->m[2][0] = dest->m[2][1] = dest->m[2][3] = 0.f;

    dest->m[3][0] = -(right + left) / (right - left);
    dest->m[3][1] = -(top + bottom) / (top - bottom);
    dest->m[3][2] = -(zfar + znear) / (zfar - znear);
    dest->m[3][3] = 1.f;
}

void mat4_print(mat4 *src) {
    printf("\naddr: %p\n", src);
    int col, row;
    for (col = 0; col < 4; col++) {
        for (row = 0; row < 4; row++) {
            printf("%f ", src->m[col][row]);
        }
        printf("\n");
    }
}
