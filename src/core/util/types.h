#ifndef TYPES_H
#define TYPES_H

#include <math.h>

#ifndef STD_BOOL_H
#define STD_BOOL_H
    typedef enum { true = 1, false = 0 } boolean;
#endif // STD_BOOL_H

#define sinf(x)  (float)sin((double)(x))
#define cosf(x)  (float)cos((double)(x))
#define tanf(x)  (float)tan((double)(x))
#define sqrtf(x) (float)sqrt((double)(x))

typedef unsigned int    u32;
typedef signed int      s32;

typedef unsigned short  u16;
typedef signed short    s16;

typedef unsigned char   byte;
typedef signed char     sbyte;

typedef union {
    float v[2];
    struct {
        float x;
        float y;
    };
} vec2;

typedef union {
    float v[3];
    struct {
        float x;
        float y;
        float z;
    };
    struct {
        float r;
        float g;
        float b;
    };
} vec3;

typedef union {
    float v[4];
    struct {
        float x;
        float y;
        float z;
        float w;
    };
    struct {
        float r;
        float g;
        float b;
        float a;
    };
} vec4;

typedef union {
    float m[4][4];
    struct {
        float m00, m10, m20, m30;
        float m01, m11, m21, m31;
        float m02, m12, m22, m32;
        float m03, m13, m23, m33;
    };
} mat4;

#endif // TYPES_H