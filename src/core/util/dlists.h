#ifndef DLIST_H
#define DLIST_H

#include <stdio.h>
#include <stdlib.h>

#include "types.h"

typedef struct {
    u16 length;
    u16 capacity;
    void **data;
} dList;

void dlist_init(dList *list, u16 memSize);
void dlist_resize(dList *dlist, u16 size);
void dlist_add(dList *dlist, void *item);
void dlist_set(dList *dlist, u16 index, void *item);
void *dlist_get(dList *dlist, u16 index);
void dlist_remove(dList *dlist, u16 index);
void dlist_free(dList *dlist);

#endif // DLIST_H
