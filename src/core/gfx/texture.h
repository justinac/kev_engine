#ifndef TEXTURE_H
#define TEXTURE_H

#include <stdio.h>
#include <glad/glad.h>

#include "../util/types.h"

typedef struct {
    u32 ID;
    u32 width;
    u32 height;
    const byte *filePath;
} Texture;

Texture texture_load(const char *filePath);

#endif // TEXTURE_H
