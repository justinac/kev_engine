#include "shader.h"

char *file_read(const char* file_path, char* mode) {
    FILE *fp = fopen(file_path, mode);

    if (!fp) {
        printf("failed to read file: %s\nerror: %d %s", file_path, errno, stderr);
        exit(EXIT_FAILURE);
    }

    fseek(fp, 0, SEEK_END);
    u32 len = ftell(fp);
    rewind(fp);
    byte *data = calloc(len + 1, sizeof(byte));
    fread(data, 1, len, fp);
    fclose(fp);
    return data;
}

u32 shader_compile(const char* filePath, int type) {
    u32 shader;
    u32 length, result;

    char* source = file_read(filePath, "r");
    if(!source) return 0;

    shader = glCreateShader(type);
    length = strlen(source);
    glShaderSource(shader, 1, (const char**)&source, &length);
    glCompileShader(shader);
    free(source);

    glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
    if(result == GL_FALSE) {
        char* log;

        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
        log = malloc(length);
        glGetShaderInfoLog(shader, length, &result, log);

        fprintf(stderr, "shaderCompileFromFile(): Unable to compile %s: %s\n", filePath, log);
        free(log);

        glDeleteShader(shader);
        return 0;
    }

    return shader;
}

u16 shader_load(const char *vertexFilePath, const char *fragmentFilePath) {
    unsigned int vertexShaderID   = shader_compile(vertexFilePath, GL_VERTEX_SHADER);
    unsigned int fragmentShaderID = shader_compile(fragmentFilePath, GL_FRAGMENT_SHADER);

	unsigned int programID = glCreateProgram();
	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID);
	glLinkProgram(programID);

	glDetachShader(programID, vertexShaderID);
	glDetachShader(programID, fragmentShaderID);

	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);

    return programID;
}

void shader_use(u32 programID) {
    glUseProgram(programID);
}