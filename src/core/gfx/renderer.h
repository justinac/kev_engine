#ifndef RENDERER_H
#define RENDERER_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "../util/types.h"
#include "../util/dlists.h"
#include "../gui/window.h"
#include "../models/scene.h"
#include "../models/entity.h"
#include "../models/camera.h"
#include "shader.h"

byte SCREEN_UNITS;

void renderer_init(void);
void renderer_clear_screen(void);
void renderer_draw(dList *list, Camera *camera, u32 shaderProgramID);

#endif // RENDERER_H
