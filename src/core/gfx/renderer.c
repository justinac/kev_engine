#include "renderer.h"

static mat4 perspective;
static mat4 view;
static mat4 mvp;

void renderer_init(void) {
    SCREEN_UNITS = 3.0f;
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);

    glEnable(GL_DEPTH_TEST);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    mat4_identity(&perspective);
    mat4_identity(&view);

    mat4_mul(&mvp, perspective, view);
}

void renderer_clear_screen(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
}

void renderer_draw(dList *list, Camera *camera, u32 shaderProgramID) {
    glUseProgram(shaderProgramID);
    glEnableVertexAttribArray(0);

    mat4_orthographic(&perspective, -window_get_width() / SCREEN_UNITS, window_get_width() / SCREEN_UNITS, window_get_height() / SCREEN_UNITS, -window_get_height() / SCREEN_UNITS, -100.0f, 100.0f);

    int i;
    for (i = 1; i < list->capacity; i++) {
        entity_update(&(*(Entity*)dlist_get(list, i)));

        mat4_mul(&mvp, perspective, camera->viewMatrix);
        mat4_mul(&mvp, mvp, (*(Entity*)dlist_get(list, i)).mesh.transformationMatrix);

        glUniformMatrix4fv(glGetUniformLocation(shaderProgramID, "mvp"), 1, GL_FALSE, &mvp.m00);

        glBindTexture(GL_TEXTURE_2D, (*(Entity*)dlist_get(list, i)).mesh.texture.ID);
        glBindVertexArray((*(Entity*)dlist_get(list, i)).mesh.vaoID);
        glDrawElements(GL_TRIANGLES, (*(Entity*)dlist_get(list, i)).mesh.indiceCount, GL_UNSIGNED_SHORT, 0);
    }

    glDisableVertexAttribArray(0);
}