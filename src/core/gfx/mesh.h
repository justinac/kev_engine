#ifndef MESH_H
#define MESH_H

#include <stdio.h>
#include <stdlib.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "../util/types.h"
#include "../util/math.h"

#include "texture.h"

typedef struct {
    vec3    size;
    u32     vaoID;
    u16     indiceCount;
    Texture texture;
    mat4    transformationMatrix;
} Mesh;

Mesh mesh_generate(Texture texture);
void mesh_update(Mesh *mesh);

#endif // MESH_H