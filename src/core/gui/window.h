#ifndef WINDOW_H
#define WINDOW_H

#include <stdio.h>
#include <stdlib.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "../util/types.h"

typedef struct {
    GLFWwindow *GLFWwindowHandle;
    u32 width;
    u32 height;
    const char *title;
} Window;

Window window_create(u32 width, u32 height, const char *title);
void window_update(Window *window);
void window_destroy(Window *window);

float window_get_width(void);
float window_get_height(void);

#endif // WINDOW_H