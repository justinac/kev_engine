#include "window.h"

void framebuffer_size_callback(GLFWwindow* window, int width, int height);

Window window_create(u32 width, u32 height, const char *title) {
    if (!glfwInit()) {
        printf("failed to initialize glfw...\n");
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    #ifdef __APPLE__
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif

    GLFWwindow *hGLFW = glfwCreateWindow(width, height, title, NULL, NULL);
    if (!hGLFW) {
        printf("failed to create glfw window...\n");
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(hGLFW);
    glfwSetFramebufferSizeCallback(hGLFW, framebuffer_size_callback);

    glfwSwapInterval(1);

    return (Window) {
        hGLFW,
        width,
        height,
        title
    };
}

void window_update(Window *window) {
    glfwSwapBuffers(window->GLFWwindowHandle);
    glfwPollEvents();
}

void window_destroy(Window *window) {
    glfwDestroyWindow(window->GLFWwindowHandle);
    glfwTerminate();
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

float window_get_width(void) {
    u32 w;
    glfwGetWindowSize(glfwGetCurrentContext(), &w, 0);
    return (float)w;
}

float window_get_height(void) {
    u32 h;
    glfwGetWindowSize(glfwGetCurrentContext(), 0, &h);
    return (float)h;
}
