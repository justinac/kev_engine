#include "core.h"

void playerController(Entity *entity) {
    if (input_get_key(GLFW_KEY_W, GLFW_PRESS)) {
        entity->velocity.y = 5.0f;
    } else if (input_get_key(GLFW_KEY_S, GLFW_PRESS)) {
        entity->velocity.y = -5.0f;
    } else {
        entity->velocity.y = 0.0f;
    }

    if (input_get_key(GLFW_KEY_A, GLFW_PRESS)) {
        entity->velocity.x = -5.0f;
        entity->scale.x = -1.0f;
    } else if (input_get_key(GLFW_KEY_D, GLFW_PRESS)) {
        entity->velocity.x = 5.0f;
        entity->scale.x = 1.0f;
    } else {
        entity->velocity.x = 0.0f;
    }
}

int main(void) {
    Window window = window_create(1280, 720, "kevinEngine");
    renderer_init();
    
    u32 shaderProgramID = shader_load(
        "../build/resources/shaders/vertex.glsl",
        "../build/resources/shaders/fragment.glsl"
    );

    Entity player = entity_create(
        mesh_generate(texture_load("../build/resources/sprites/playerMove.png")),
        (vec3){ 0.0f, 0.0f, 0.0f },
        (vec3){ 1.0f, 1.0f, 1.0f }
    );

    Entity boss = entity_create(
        mesh_generate(texture_load("../build/resources/sprites/bossMove.png")),
        (vec3){ 0.0f, 250.0f, 0.0f },
        (vec3){ 1.0f, 1.0f, 1.0f }
    );

    Entity wall = entity_create(
        mesh_generate(texture_load("../build/resources/sprites/wall.png")),
        (vec3){ 0.0f, 0.0f, 0.0f },
        (vec3){ 1.0f, 1.0f, 1.0f }
    );

    dList list;
    dlist_init(&list, sizeof(Entity));
    dlist_add(&list, &wall);
    dlist_add(&list, &boss);
    dlist_add(&list, &player);

    Camera camera = camera_create((vec3){ 0.0f, 0.0f, 0.0f });

    double lastTime = glfwGetTime();
    double timePerTick = 1.0f / 120.0f;

    int i = 0;
    double xpos, ypos;
    while (!glfwWindowShouldClose(window.GLFWwindowHandle)) {
        window_update(&window);

        if (glfwGetTime() >= lastTime + timePerTick) {
            lastTime = glfwGetTime();

            if (player.position.y < boss.position.y - (boss.size.y / 2.0f)) {
                player.position.z = 1.0f;
            } else {
                player.position.z = -1.0f;
            }

            double xpos, ypos;
            glfwGetCursorPos(glfwGetCurrentContext(), &xpos, &ypos);
            xpos =  ((xpos - window_get_width())  / SCREEN_UNITS) + player.position.x;
            ypos = -((ypos - window_get_height()) / SCREEN_UNITS) + player.position.y;
            wall.position.x = xpos;
            wall.position.y = ypos;

            playerController(&player);
            camera_lock(&camera, player.position);
        }

        renderer_clear_screen();
        renderer_draw(&list, &camera, shaderProgramID);
    }

    window_destroy(&window);

    return 0;
}
